import './App.css';
import React, {useEffect, useState, useRef} from "react";
import {Miracle} from "./miracle";
import {Circle, PolyShape, Image} from "./miracle/entity";
import {Point} from "./miracle/graphic";
import {ControlBase, ImageControl} from './miracle/control';

const App = () => {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const [miracle, setMiracle] = useState<Miracle>();

    // entity
    const [rect, setRect] = useState<PolyShape>();
    const [triangle, setTriangle] = useState<PolyShape>();
    const [circle, setCircle] = useState<Circle>();
    const [img, setImage] = useState<Image>();

    // 图形可见
    const [imgVisible, setImageVisible] = useState(true);
    const [circleVisible, setCircleVisible] = useState(true);
    const [rectVisible, setRectVisible] = useState(true);
    const [triangleVisible, setTriangleVisible] = useState(true);

    // 控制点
    const [xctrShow, setXctrShow] = useState(true);
    const [yctrShow, setYctrShow] = useState(true);
    const [diagctrShow, setDiagctrShow] = useState(true);
    const [rotatectrShow, setRotatectrShow] = useState(true);

    useEffect(() => {
        const canvas = canvasRef.current;
        if (canvas) {
            const sizew = 600;
            const sizeh = 600;
            canvas.width = sizew;
            canvas.height = sizeh;
            canvas.style.width = `${sizew}px`;
            canvas.style.height = `${sizeh}px`;
            setMiracle(new Miracle(canvas, [], false, true));
        }
    }, [canvasRef]);

    useEffect(() => {
        if (rect) {
            rect.visible = rectVisible;
        }
        if (triangle) {
            triangle.visible = triangleVisible;
        }
        if (img) {
            img.visible = imgVisible;
        }
        if (circle) {
            circle.visible = circleVisible;
        }
        miracle?.redraw();
    }, [imgVisible, circleVisible, rectVisible, triangleVisible, rect, triangle, img, circle, miracle]);

    useEffect(() => {
        if (miracle) {
            miracle.xLocked = !xctrShow;
            miracle.yLocked = !yctrShow;
            miracle.diagLocked = !diagctrShow;
            miracle.rotateLocked = !rotatectrShow;
        }
    }, [diagctrShow, miracle, rotatectrShow, xctrShow, yctrShow]);

    useEffect(() => {
        if (miracle) {
            const btnLt = new ImageControl("/aiming.png", {width: 30, height: 30}, ControlBase.lt, -30, -30);
            const btnLm = new ImageControl("/accept-email.png", {width: 30, height: 30}, ControlBase.lm, -30, -15);
            const btnLd = new ImageControl("/airplay.png", {width: 30, height: 30}, ControlBase.ld, -30, 0);
            const btnMd = new ImageControl("/alarm.png", {width: 30, height: 30}, ControlBase.md, -15, 0);
            const btnRd = new ImageControl("/api.png", {width: 30, height: 30}, ControlBase.rd, 0, 0);
            const btnRm = new ImageControl("/bug.png", {width: 30, height: 30}, ControlBase.rm, 0, -15);
            const btnRt = new ImageControl("/click.png", {width: 30, height: 30}, ControlBase.rt, 0, -30);
            const btnMt = new ImageControl("/clear.png", {width: 30, height: 30}, ControlBase.mt, -15, -30);

            btnLt.mouseDownHandler = () => {
                console.log("You click the buttonlt");
            };
            btnLt.mouseUpHandler = () => {
                console.log("===========dataURL==========");
                // console.log(btnLt.owner?.toDataUrl("image/png", {x: 1, y: 2}));
                console.log(miracle.toDataUrl("image/png", {x: 1, y: 0.5}));
            }

            btnMt.mouseUpHandler = () => {
                if (btnMt.owner) {
                    // miracle.removeEntity(btnMt.owner);
                    miracle.removeAll();
                }
            }

            btnMt.touchStart = () => {
                alert("hello js");
            }

            btnMt.touchEnd = () => {
                alert("hello world");
            };

            const rect = new PolyShape([
                new Point(150, 30),
                new Point(200, 30),
                new Point(200, 120),
                new Point(150, 120)
            ], false);
            rect.filled = false;
            rect.closed = true;
            rect.addControl(btnLm, btnLd, btnMd, btnRd, btnRm, btnRt, btnMt);

            const circle = new Circle(new Point(400, 400), 50);
            circle.strokeStyle = "green";
            circle.addControl(btnLt);

            const triangle = new PolyShape([
                new Point(100, 100),
                new Point(150, 150),
                new Point(100, 200)
            ]);
            triangle.filled = true;
            triangle.closed = true;
            triangle.fillStyle = "gray";

            const img = new Image(new Point(200, 300), "/logo192.png", {
                height: 150,
                width: 180
            });
            miracle.limitInCanvas = true;
            miracle.addEntity(circle, rect, triangle, img);
            setRect(rect);
            setTriangle(triangle);
            setImage(img);
            setCircle(circle);
        }
    }, [miracle]);

    return (
        <>
            <div>
                <canvas className="canvas" ref={canvasRef} />
            </div>
            <div>
                <input id="rect-checkbox" type="checkbox" onClick={() => {setRectVisible(visible => !visible)}} checked={rectVisible} /><label htmlFor="rect-checkbox">矩形</label>
                <input id="triangle-checkbox" type="checkbox" onClick={() => {setTriangleVisible(visible => !visible)}} checked={triangleVisible} /><label htmlFor="triangle-checkbox">三角形</label>
                <input id="circle-checkbox" type="checkbox" onClick={() => {setCircleVisible(visible => !visible)}} checked={circleVisible} /><label htmlFor="circle-checkbox">圆形</label>
                <input id="image-checkbox" type="checkbox" onClick={() => {setImageVisible(visible => !visible)}} checked={imgVisible} /><label htmlFor="image-checkbox">图片</label>
            </div>
            <div>
                <input type="checkbox" onClick={() => {setXctrShow(show => !show)}} checked={xctrShow} /><label>X控制</label>
                <input type="checkbox" onClick={() => {setYctrShow(show => !show)}} checked={yctrShow} /><label>Y控制</label>
                <input type="checkbox" onClick={() => {setDiagctrShow(show => !show)}} checked={diagctrShow} /><label>对角线控制</label>
                <input type="checkbox" onClick={() => {setRotatectrShow(show => !show)}} checked={rotatectrShow} /><label>旋转控制</label>
            </div>
        </>
    );
}

export default App;
